//
//  UserData.swift
//  MyFirstSwiftUI
//
//  Created by Mostafa Sandeed on 3/10/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import SwiftUI

final class UserData: ObservableObject {
    @Published var showFavoritesOnly: Bool = false
    @Published var landmarks = landmarkData
}
